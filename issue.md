# 总结
## 1. vue3项目中如何支持jsx
   a.需要下载安装@vitejs/plugin-vue-jsx
   b.在vite.config.js中的plugin配置vueJsx
## 2.如何在vue3项目中实现css in js
   a.下载vue-jss
   b.具体使用
   ```javascript
   import {createUseStyles} from "vue-jss"

    //css样式
    const useStyles = createUseStyles({
        menuButton:{
            padding:15,
        }
    })`
   ```
## 3.编辑器解析代码
```javascript
    npm install monaco-editor -s
```
## 4.问题vite没有对应的monaco-editor-webpack-plugin插件，所以，如果直接使用并初始化，可以看到效果，但是会控制台报错Error: Unexpected usage at EditorSimpleWorker.loadForeignModule
引入相应的worker即可解决，初始化之前设置环境

## 5.解决vscode里面json中不允许写注释的问题
点击工具栏json，在弹出的窗口输入json with comments，找到后点击，即可解决红色报错提示

## 6.注意单独下载vue-jss，修改可能会出现的问题不生效的问题
如果要使用css in js,需要使用jss、jss-preset-default以及vue-jss结合使用

## 7.如果从git下载开源项目，可能会碰见的问题,执行npm install 可能遇见的问题 gyp ERR find Python
npm install --global --production windows-build-tools

## 8.注意yarn add 会报错Missing list of packages to add to your project
  一般yarn add + 包名,才能下载成功，如果想要安装全部依赖，直接使用yarn install

## 9.yarn 安装包时报: certificate has expired
  查看strict-ssl，可以查看列表 yarn config list
  然后再通过yarn config set strict-ssl false即可