import { defineComponent } from "vue"
import { FiledPropsDefine } from "../types"
import { useVJSFContext } from "../context"
import { isObject } from "../utils"

export default defineComponent({
  name:'ObjectField',
  props:FiledPropsDefine,
  setup(props) {
    const context = useVJSFContext()

    const handleObjectFieldChange = (key:string,v:any)=>{
      const value:any = isObject(props.value)?props.value:{}
      //如果返回值是undefined，则清除这个字段，否则进行修改
      if(v===undefined){
        delete value[key]
      }else{
        value[key] = v
      }
      props.onChange(value)
    }

    return ()=>{
      const {schema,rootSchema,value} = props
      //获取SchemaItem
      const {SchemaItem} = context;
      const properties = schema.properties||{}
      const currentValue:any = isObject(value)?value:{}

      return Object.keys(properties).map((k:string,index:number)=>{
        return(
          <SchemaItem 
            schema={properties[k]}
            rootSchema = {rootSchema}
            value = {currentValue[k]}
            key = {index}
            onChange={(v:any)=>handleObjectFieldChange(k,v)}
          />
        )
      })
    }
  },
})