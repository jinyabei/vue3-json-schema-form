import { defineComponent } from "vue"
import { FiledPropsDefine, Schema } from "../types"
import { useVJSFContext } from "../context"
import ArrayItemWrapper from './ArrayItemWrapper'
import SelectionWidget  from '../widgets/Selection'

export default defineComponent({
  name:'ArrayField',
  props:FiledPropsDefine,
  setup(props) {
    //数组的其中一项有修改
    const handleArrayItemChange = (v:any,index:number)=>{
      const {value} = props;
      const arr = Array.isArray(value)?value:[]
      arr[index] = v;
      props.onChange(arr)
    }

    //新增数据
    const handleAdd = (index:number)=>{
      const {value} = props
      const arr = Array.isArray(value)?value:[]
      arr.splice(index+1,0,undefined)
      props.onChange(arr)
    }

    //删除数据
    const handleDelete = (index:number)=>{
      const {value} = props
      const arr = Array.isArray(value)?value:[]
      arr.splice(index,1)
      props.onChange(arr)
    }

    //上升数据
    const handleUp = (index:number)=>{
      if(index===0)return;
      const{value}= props
      const arr = Array.isArray(value)?value:[]
      const item = arr.splice(index,1)
      arr.splice(index-1,0,item[0])
      props.onChange(arr)
    }

    //下降数据
    const handleDown = (index:number)=>{
      const {value}= props
      const arr = Array.isArray(value)?value:[]
      if(index==arr.length-1)return;
      const item = arr.splice(index,1)
      arr.splice(index+1,0,item[0])
      props.onChange(arr)
    }

    return ()=>{
      const {schema,rootSchema,value} = props

      const context = useVJSFContext()
      const SchemaItem = context.SchemaItem

      //如果items是数组
      const isMultiType = Array.isArray(schema.items)
      // todo:待写注释
      const isSelect = schema.items&&(schema.items as any).enum
      if(isMultiType){
        const items:Schema[] = schema.items as any
        const arr = Array.isArray(value)?value:[]
        return items.map((s:Schema,index:number)=>{
          return(
            <SchemaItem 
              schema={s}
              rootSchema={rootSchema}
              key={index}
              value = {arr[index]}
              onChange ={((v:any)=>handleArrayItemChange(v,index))}
            />
          )
        })
      }else if(!isSelect){
        const arr = Array.isArray(value)?value:[]
        return arr.map((v:any,index:number)=>{
          return(
            <ArrayItemWrapper 
              index={index} 
              onAdd={handleAdd}
              onDelete = {handleDelete}
              onDown = {handleDown}
              onUp = {handleUp}
            >
              <SchemaItem 
                schema={schema.items as Schema} 
                value = {v} 
                key={index}
                rootSchema={rootSchema}
                onChange = {(v:any)=>handleArrayItemChange(v,index)}
              />
            </ArrayItemWrapper>
          )
        })
      }else{
        const enumOptions = (schema as any).items.enum
        const options = enumOptions.map((e:any)=>({
          key:e,
          value:e
        }))
        return (
          <SelectionWidget 
            onChange={props.onChange} 
            value={props.value} 
            options={options}/>
        )
      }
    }
  },
})