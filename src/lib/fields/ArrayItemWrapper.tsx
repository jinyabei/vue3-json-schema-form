import { PropType, defineComponent } from "vue"
import { createUseStyles } from "vue-jss"

const useStyles = createUseStyles({
  container:{
    border:"1px solid #eee",
  },
  actions:{
    background:'#eee',
    padding:10,
    textAlign:'right'
  },
  action:{
    '&+&':{
      marginLeft:10
    }
  },
  content:{
    padding:10
  }
})

export default defineComponent({
  name:'ArrayItemWrapper',
  props:{
    index:{
      type:Number,
      required:true,
    },
    onAdd:{
      type:Function as PropType<(index:number)=>void>,
      required:true,
    },
    onDelete:{
      type:Function as PropType<(index:number)=>void>,
      required:true,
    },
    onUp:{
      type:Function as PropType<(index:number)=>void>,
      required:true,
    },
    onDown:{
      type:Function as PropType<(index:number)=>void>,
      required:true,
    }
  },
  setup(props,{slots}) {
    const classesRef = useStyles()

    const handleAdd = ()=>props.onAdd(props.index)
    const handleDelete = ()=>props.onDelete(props.index)
    const handleUp = ()=>props.onUp(props.index)
    const handleDown = ()=>props.onDown(props.index)

    return ()=>{
      const classes = classesRef.value
      return (
        <div class={classes.container}>
          <div class={classes.actions}>
            <button class={classes.action} onClick={handleAdd}>新增</button>
            <button class={classes.action} onClick={handleDelete}>删除</button>
            <button class={classes.action} onClick={handleUp}>上移</button>
            <button class={classes.action} onClick={handleDown}>下移</button>
          </div>
          <div class={classes.content}>{slots.default&&slots.default()}</div>
        </div>
      )
    }
  }
})