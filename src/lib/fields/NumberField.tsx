import { defineComponent } from "vue"
import { FiledPropsDefine } from "../types"

export default defineComponent({
  name:'NumberField',
  props:FiledPropsDefine,
  setup(props) {
    const handleChange = (e:any)=>{
      const value = e.target.value
      //装换成数字
      const num = Number(value)
      if(Number.isNaN(num)){
        props.onChange(undefined)
      }else{
        props.onChange(num)
      }
    }

    return ()=>{
      const {value} = props
      return <input value={value} type="number" onInput={handleChange}/>
    }
  },
})