import { PropType, defineComponent, provide } from "vue";
import SchemaItem from "./SchemaItem"
import { Schema } from "./types";
import { SchemaFormContextKey } from './context';

export default defineComponent({
  name:'SchemaForm',
  props:{
    schema:{
      type:Object as PropType<Schema>,
      required:true,
    },
    value:{
      required:true
    },
    onChange:{
      type:Function as PropType<(value:any)=>void>,
      required:true,
    }
  },
  setup(props) {
    const handleChange = (value:any)=>{
      props.onChange(value)
    }

    const context:any = {
      SchemaItem
    }

    provide(SchemaFormContextKey,context)

    return ()=>{
      const {schema,value} = props
      return(
        <SchemaItem 
          schema={schema}
          rootSchema = {schema}
          value = {value}
          onChange = {handleChange}
        />
      )
    }  
  }, 
})