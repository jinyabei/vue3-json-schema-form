import { defineComponent,computed } from "vue";
import { SchemaTypes,FiledPropsDefine } from "./types";
//引入组件
import NumberField from './fields/NumberField'
import StringField from './fields/StringField'
import ObjectField from './fields/ObjectField'
import ArrayField from './fields/ArrayField'

import {retrieveSchema} from './utils'

export default defineComponent({
  name:'SchemaItem',
  props:FiledPropsDefine,
  setup(props) {
    const retrievedSchemaRef = computed(()=>{
      const {schema,rootSchema,value} = props
      return retrieveSchema(schema,rootSchema,value)
    })

    return ()=>{
      const {schema} =props
      const retrievedSchema = retrievedSchemaRef.value

      //获取组件类型
      const type = schema.type
      //组件名
      let Component:any;

      switch(type){
        case SchemaTypes.NUMBER:{
          Component = NumberField
          break;
        }
        case SchemaTypes.STRING:{
          Component = StringField
          break;
        }
        case SchemaTypes.OBJECT:{
          Component = ObjectField
          break;
        }
        case SchemaTypes.ARRAY:{
          Component = ArrayField
          break;
        }
        default:{
          console.log(`${type} is not supported`)
        }
      }

      return <Component {...props} schema={retrievedSchema}/>
    }
  },
})