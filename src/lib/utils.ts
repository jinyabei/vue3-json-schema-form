/* eslint-disable no-prototype-builtins */
import jsonpointer from 'jsonpointer';
//合并数组
import union from 'lodash.union'
import Ajv from 'ajv'

import mergeAllOf from "json-schema-merge-allof"

import { Schema } from "./types"

//判断是否是空对象
export function isObject(schema:any){
  return typeof schema=='object'&&schema!==null&&!Array.isArray(schema)
}

//是否自有属性
export function hasOwnProperty(schema:any,key:string){
  /**
   * 直接调用`schema.hasOwnProperty`有可能会因为
   * schema 覆盖了 prototype 上的 hasOwnProperty 而产生错误
   */
  return Object.prototype.hasOwnProperty.call(schema,key)
}

/**
 * 处理带有$ref格式的数据
 * @param schema 
 * @param rootSchema 
 * @param formData 
 * @returns 
 */
export function resolveReference(schema:any,rootSchema:any,formData:any){
  //找到当前的$ref值
  const $refSchema = findSchemaDefinition(schema.$ref,rootSchema)
  const {$ref,...localSchema} = schema;
  return retrieveSchema({
    ...$refSchema,
    ...localSchema
  },rootSchema,formData)
}

/**
 * 找到当前ref的值
 * @param $ref 当前$Ref
 * @param rootSchema 
 * @returns 
 */
export function findSchemaDefinition($ref:string,rootSchema={}):Schema{
  const origRef = $ref;
  //如果以#开头,去掉#号,否则报错
  if($ref.startsWith("#")){
    $ref = decodeURIComponent($ref.substring(1))
  }else{
    throw new Error(`Could not find a definiton for ${origRef}`)
  }
  //得到当前$ref的值
  const current = jsonpointer.get(rootSchema,$ref);
  //$ref值为undefined报错
  if(current===undefined){
    throw new Error(`could not find a definiton for ${origRef}`)
  }
  //$ref值不为undefined，且是原型上的属性
  if(hasOwnProperty(current,'$ref')){
    return findSchemaDefinition(current.$ref,rootSchema)
  }
  return current;
}

export function resolveDependencies(schema:any,rootSchema:any,formData:any):Schema{
  // eslint-disable-next-line prefer-const
  let { dependencies = {}, ...resolvedSchema } = schema
  //根据规则处理相关数据
  if('oneOf' in resolvedSchema){
    resolvedSchema = resolvedSchema.oneOf[getMatchingOption(formData,resolvedSchema.oneOf,rootSchema)]
  }else if('anyOf' in resolvedSchema){
    resolvedSchema = resolvedSchema.anyOf[getMatchingOption(formData,resolvedSchema.anyOf,rootSchema)]
  }
  return processDependencies(dependencies,resolveSchema,rootSchema,formData)
}

/**
 * 增强schema
 * @param formData 
 * @param options 
 * @param isValid 
 * @returns 
 */
export function getMatchingOption(formData:any,options:Schema[],isValid:(schema:Schema,data:any)=>boolean){
  for(let i =0;i<options.length;i++){
    const option = options[i]
    //每一项拥有属性
    if(option.properties){
      const requiresAnyOf={
        anyOf:Object.keys(option.properties).map(key=>({
          required:[key]
        }))
      }
      //增强的schema
      let augmentedSchema
      //属性中是否有anyoof
      if(option.anyOf){
        const {...shallowClone} = option
        //属性中不存在allof，则为[],存在则进行切割
        if(!shallowClone.allOf){
          shallowClone.allOf = []
        }else{
          shallowClone.allOf = shallowClone.allOf.slice()
        }
        //再加上基础配置
        shallowClone.allOf.push(requiresAnyOf)
        augmentedSchema = shallowClone
      }else{
        augmentedSchema = Object.assign({},option,requiresAnyOf)
      }
      delete augmentedSchema.required
      if(isValid(options[i],formData)){
        return i;
      }
    }else if(isValid(options[i],formData)){
      return i;
    }
  }
  return 0;
}

function processDependencies(dependencies:any,resolvedSchema:any,rootSchema:any,formData:any):Schema{
 for(const dependencyKey in dependencies){
  //停止当前语句,如果当前数据，无当前dependencyKey值,则不处理
  if(formData[dependencyKey]===undefined){
    continue
  }
  //如果冲突的schema中的属性无当前dependencyKey值,则不处理
  if(resolvedSchema.properties&&!(dependencyKey in resolvedSchema.properties)){
    continue;
  }
  
  //如果有，则取出dependencyKey的值和其他值,如果 dependencyValue是数组和对象，则单独处理
  const {
    [dependencyKey]:dependencyValue,
    ...remainingDependenecies
  } = dependencies
  if(Array.isArray(dependencyValue)){
    resolvedSchema = withDependentProperties(resolvedSchema,dependencyValue)
  }else if(isObject(dependencyValue)){
    resolvedSchema = withDependentSchema(
      resolvedSchema,
      rootSchema,
      formData,
      dependencyKey,
      dependencyValue
    )
  }
  return processDependencies(
    remainingDependenecies,
    resolvedSchema,
    rootSchema,
    formData,
  )
 }
 return resolvedSchema
}

/**
 * 区分schema和required
 * @param schema 
 * @param additionallyRequired 
 * @returns 
 */
function withDependentProperties(schema:any,additionallyRequired:any){
  if(!additionallyRequired){
    return schema
  }
  const required = Array.isArray(schema.required)?Array.from(new Set([...schema.required,...additionallyRequired])):additionallyRequired
  return {...schema,required:required}
}

/**
 * 处理oneOf以及oneOf中含有ref的情况
 * @param schema 
 * @param rootSchema 
 * @param formData 
 * @param dependencyKey 
 * @param dependencyValue 
 * @returns 
 */
function withDependentSchema(schema:any,rootSchema:any,formData:any,dependencyKey:any,dependencyValue:any){
  const {oneOf,...dependentSchema} = retrieveSchema(
    dependencyValue,
    rootSchema,
    formData,
  )
  schema = mergeSchemas(schema,dependentSchema)
  if(oneOf===undefined){
    return schema
  }else if(!Array.isArray(oneOf)){
    throw new Error(`invalid:it is some ${typeof oneOf} instead of an array`)
  }
  //如果oneOf是数组，判断oneOf里面是否含有$ref
  const resolvedOneOf = oneOf.map((subschema)=>{
    hasOwnProperty(subschema,'$ref')?resolveReference(subschema,rootSchema,formData)
    :subschema
  })
  return withExactlyOneSubschema(
    schema,
    rootSchema,
    formData,
    dependencyKey,
    resolvedOneOf,
  )
}

function withExactlyOneSubschema(schema:any,rootSchema:any,formData:any,dependencyKey:any,oneOf:any){
  const validSubschemas = oneOf.filter((subschema:any)=>{
    if(!subschema.properties){
      return false
    }
    const {[dependencyKey]:conditionPropertySchema} = subschema.properties
    if(conditionPropertySchema){
      const conditionSchema = {
        type:Object,
        properties:{
          [dependencyKey]:conditionPropertySchema,
        }
      }
      const {errors} = validateData(conditionSchema,formData)
      return !errors||errors.length===0
    }
  })
  if(validSubschemas.length!==1){
    console.log("ignoring oneOf in dependencies because there is not exactly one subschema that is valid")
    return schema
  }
  const subschema = validSubschemas[0]
  const {
    [dependencyKey]:conditionPropertySchema,
    ...dependentSubschema
  } = subschema.properties
  const dependentSchema = {
    ...subschema,
    properties:dependentSubschema
  }
  return mergeSchemas(
    schema,
    retrieveSchema(dependentSchema,rootSchema,formData)
  )
}

/**
 * 合并两个对象
 * @param obj 
 * @param currentObj 
 * @returns 
 */
export function mergeSchemas(obj:any,currentObj:any){
  const acc = Object.assign({},obj)
  return Object.keys(currentObj).reduce((prev,key)=>{
    const left = obj?obj[key]:{},
          right = currentObj[key]
    //如果两个对象都有对应的可以值，且都是对象，则合并两个对象,如果都是数字，则合并数组，否则赋值为当前数值
    if(obj&&hasOwnProperty(obj,key)&&isObject(right)){
      prev[key] = mergeSchemas(left,right)
    }else if(obj&&currentObj&&(getSchemaType(obj)==='object'||getSchemaType(currentObj)==='object')&&key=='required'&&
    Array.isArray(left)&&Array.isArray(right)){
      prev[key] = union(left,right)
    }else{
      prev[key] = right
    }
    return prev;
  },acc)
}

/**
 * 判断schema的类型
 * @param schema 
 * @returns 
 */
export function getSchemaType(schema:Schema):string|undefined{
  const {type} = schema
  //常量、枚举
  if(!type&&schema.const){
    return guessType(schema.const)
  }
  if(!type&&schema.enum){
    return 'string'
  }
  //属性或者额外属性
  if(!type&&(schema.properties||schema.additionalProperties)){
    return 'object'
  }
  //去除数组中的null值
  const t:any = type
  if(t instanceof Array&&t.length===2&&t.includes("null")){
    return t.find((type)=>type!=='null')
  }
  return type
}

const defaultInstance = new Ajv();
/**
 * 返回校验是否通过和校验错误信息
 * @param schema 
 * @param data 
 * @returns 
 */
export function validateData(schema:any,data:any){
  const valid =defaultInstance.validate(schema,data)
  return{
    valid,
    errors:defaultInstance.errors,
  }
}

/**
 * 返回处理后的schama
 * @param schema schema
 * @param rootSchema 根schema
 * @param formData 数据
 * @returns 
 */
export function resolveSchema(schema:Schema,rootSchema={},formData={}):Schema{
  //检测$ref是否自有属性
  if(hasOwnProperty(schema,'$ref')){
    //返回当前$ref的值
    return resolveReference(schema,rootSchema,formData)
  }else if(hasOwnProperty(schema,'dependencies')){
    //检测dependencies是否自有属性
    const resolvedSchema = resolveDependencies(schema,rootSchema,formData)
    return retrieveSchema(resolvedSchema,rootSchema,formData)
  }else if(hasOwnProperty(schema,'allOf')&&Array.isArray(schema.allOf)){
    //检测是否有allof属性，且allOf是否为数组，解析出allof中的每一项
    return {
      ...schema,
      allOf:schema.allOf.map((allOfSubschema)=>{
        return retrieveSchema(allOfSubschema,rootSchema,formData)
      })
    }
  }else{
    return schema;
  }
}

export const ADDITIONAL_PROPERTY_FLAG = '__additional_property';

/**
 * 返回符合要求的type类型
 * @param value 
 * @returns 
 */
export const guessType = function guessType(value:any){
  if(Array.isArray(value)){
    return 'array'
  }else if(typeof value=='string'){
    return 'string'
  }else if(value==null){
    return 'null'
  }else if(typeof value =='boolean'){
    return 'boolean'
  }else if(!isNaN(value)){
    return 'number'
  }else if(typeof value==='object'){
    return 'object'
  }
  return 'string'
}

/**
 * 处理含有附加属性的对象
 * @param schema 
 * @param rootSchema 
 * @param formData 
 * @returns 
 */
export function stubExistingAdditionalProperties(
  schema:Schema,
  rootSchema:Schema={},
  formData:any = {}
){
  schema = {
    ...schema,
    properties:{...schema.properties}
  }

  Object.keys(formData).forEach((key)=>{
    //schema不为undefined,schema和数据中的属性一致，不做任何处理
    if((schema as any).properties.hasOwnProperty(key)){
      return;
    }
    //如果schema的附加属性中有$ref,type，按照指定格式解析出来,如果是其他格式,则指定type类型,并且设置其他属性为true
    let additionalProperties
    if(schema.additionalProperties.hasOwnProperty('$ref')){
      additionalProperties = retrieveSchema(
        {$ref:schema.additionalProperties['$ref']},
        rootSchema,
        formData,
      )
    }else if(schema.additionalProperties.hasOwnProperty('type')){
      additionalProperties = {
        ...schema.additionalProperties
      }
    }else{
      additionalProperties = {type:guessType(formData[key])}
    }
    (schema as any).properties[key] = additionalProperties
    (schema as any).properties[key][ADDITIONAL_PROPERTY_FLAG] = true
  })
  return schema
}

/**
 * 获取处理后的schema
 * @param schema schema
 * @param rootSchema 根schema
 * @param formData 数据
 * @returns 
 */
export function retrieveSchema(schema:any,rootSchema={},formData:any={}):Schema{
  //如果是个对象，且不是null和数组，即为空对象，则返回{}
  if(!isObject(schema)){
    return {} as Schema
  }
  //获取处理后的schema
  let resolvedSchema = resolveSchema(schema,rootSchema,formData)
  //如果有allOf,则进行合并操作,处理数据
  if('allOf' in schema){
    try{
      resolvedSchema = mergeAllOf({
        ...resolvedSchema,
        allOf:resolvedSchema.allOf,
      }as any) as Schema
    }catch(e){
      console.warn('could not merge subschemas in allOf:\n' + e)
      const { allOf,...resolvedSchemaWithoutAllOf} = resolvedSchema
      return resolvedSchemaWithoutAllOf
    }
  }
  //拥有附加属性(additionalProperties),且值不为false,即为全部解析
  const hasAdditionalProperties = 
    resolvedSchema.hasOwnProperty('additionalProperties')&&resolvedSchema.additionalProperties!==false
  if(hasAdditionalProperties){
    return stubExistingAdditionalProperties(
      resolvedSchema,
      rootSchema,
      formData
    )
  }
  return resolvedSchema
}