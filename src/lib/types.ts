import { PropType, defineComponent } from "vue";

//定义可渲染的组件类型
export enum SchemaTypes {
  'NUMBER' = 'number',
  'STRING' = 'string',
  'OBJECT' = 'object',
  'ARRAY' = 'array'
}

type SchemaRef = {$ref:string}

export interface Schema{
  //组件类型
  type?:SchemaTypes|string
  //常量
  const?:any
  //标题、默认值
  title?:string
  default?:any

  //组件属性
  properties?:{
    [key:string]:Schema
  }
  //依赖
  dependencies?:{
    [key:string]:string[]|Schema|SchemaRef
  }

  items?:Schema|Schema[]|SchemaRef
  oneOf?:Schema[]
  anyOf?:Schema[]
  allOf?:Schema[]

  required?:string[]

  enum?:any[],//枚举值

  //附加属性和附加值
  additionalProperties?:any,
  additionalItems?:Schema,
}

//定义props的类型 （as const 强制表达式不可变）
export const FiledPropsDefine = {
  schema:{
    type:Object as PropType<Schema>,
    required:true,
  },
  rootSchema:{
    type:Object as PropType<Schema>,
    required:true,
  },
  value:{
    required:true
  },
  onChange:{
    type:Function as PropType<(v:any)=>void>,
    required:true
  }
}as const

export const TypeHelperComponent = defineComponent({
  props:FiledPropsDefine,
})

export type CommonFieldType =  typeof TypeHelperComponent