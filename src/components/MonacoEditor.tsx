import { PropType, defineComponent, onBeforeUnmount, onMounted, ref, shallowRef, watch } from "vue";
import { createUseStyles } from "vue-jss";
import * as Monaco from 'monaco-editor';
//引入相应worker,防止控制台报错
import editorWorker from 'monaco-editor/esm/vs/editor/editor.worker?worker'
import jsonWorker from 'monaco-editor/esm/vs/language/json/json.worker?worker'
import cssWorker from 'monaco-editor/esm/vs/language/css/css.worker?worker'
import htmlWorker from 'monaco-editor/esm/vs/language/html/html.worker?worker'
import tsWorker from 'monaco-editor/esm/vs/language/typescript/ts.worker?worker'


const useStyles = createUseStyles({
  container:{
    display:'flex',
    border:'1px solid #eee',
    flexDirection:'column',
    borderRadius: 5
  },
  title:{
    background:"#eee",
    padding:"10px 0",
    paddingLeft:20,
  },
  code:{
    flexGrow:1
  }
})

// 解决编辑,控制台报错提示EditorSimpleWorker.loadForeignModule问题
self.MonacoEnvironment = {
  getWorker(_, label) {
    if (label === 'json') {
      return new jsonWorker()
    }
    if (label === 'css' || label === 'scss' || label === 'less') {
      return new cssWorker()
    }
    if (label === 'html' || label === 'handlebars' || label === 'razor') {
      return new htmlWorker()
    }
    if (label === 'typescript' || label === 'javascript') {
      return new tsWorker()
    }
    return new editorWorker()
  }
}

export default defineComponent({
  props:{
    code:{
      type:String as PropType<string>,
      required:true,
    },
    title:{
      type:String as PropType<string>,
      required:true,
    },
    onChange:{
      type:Function as PropType<(value:string,event:Monaco.editor.IModelContentChangedEvent)=>void>,
      required:true
    }
  },

  setup(props){
    const classesRef = useStyles()

    //编辑器ref shallowRef对传入的值进行浅层次的处理
    const editorRef = shallowRef();
    //内容ref
    const containerRef = ref();

    let _subscription: Monaco.IDisposable | undefined
    let __prevent_trigger_change_event = false

    onMounted(()=>{
      const editor = editorRef.value = Monaco.editor.create(containerRef.value,{
        value:props.code,
        language:'json',
        formatOnPaste:true,//启用粘贴时格式
        tabSize:2,
        minimap:{
          enabled:false,
        }
      })

      //监听编辑器的修改,回传修改之后的值
      _subscription = editor.onDidChangeModelContent((event) => {
        //修改完成后,再次进行回调
        if (!__prevent_trigger_change_event) {
          props.onChange(editor.getValue(), event);
        }
      });
    })

    onBeforeUnmount(()=>{
      //如果监听存在，销毁相应的监听
      if(_subscription){
        _subscription.dispose()
      }
    })

    watch(()=>props.code,(value)=>{
      //监听code的变化，比较编辑器的新旧值，若值不一样,进行数据更新
      const editor = editorRef.value
      const model = editor.getModel();
      if(value!==model.getValue()){
        editor.pushUndoStop();
        __prevent_trigger_change_event = true
        //pushEditOperations操作之后如果需要回到之前的状态,可以回到之前的状态
        model.pushEditOperations(
          [],
          [
            {
              range:model.getFullModelRange(),
              text:value
            }
          ]
        )
        editor.pushUndoStop()
        __prevent_trigger_change_event = false
      }
    })
    
    return()=>{
      const classes = classesRef.value
      return(
        <div class={classes.container}>
          <div class={classes.title}>
            <span>{props.title}</span>
          </div>
          <div class={classes.code} ref={containerRef}></div>
        </div>
      )
    }
  }
})