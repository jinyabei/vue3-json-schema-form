import { defineComponent, reactive, ref, watchEffect, type Ref } from "vue";
//在vue中实现js的css
import { createUseStyles } from "vue-jss"
//引入工具类
import {toJson} from './util/index'
//引入可配置组件的json
import demos from './demos'
//引入自定义组件
import MonacoEditor from './components/MonacoEditor'
//引入解析组件
import SchemaForm from "./lib";

//css样式
const useStyles = createUseStyles({
  container:{
    margin:'0 auto',
    height:'100%',
    width:'1200px',
    diplay:"flex",
    flexDirection:"column"
  },
  menu:{
    marginBottom:20,
  },
  code:{
    width:700,
    flexShrink:0,
  },
  codePanel:{
    minHeight:400,
    marginBottom:20,
  },
  uiAndValue:{
    display: 'flex',
    justifyContent: 'space-between',
    flexGrow:1,
    '& > *': {
      width: '46%',
    },
  },
  menuButton:{
    borderWidth:0,
    padding:15,
    cursor:'pointer',
    background:'transparent',
    borderRadius:5,
    // appearance:'none',
    // display:'inline-block',
    '&:hover':{
      background:'#efefef'
    }
  },
  menuSelected:{
    background:"#337ab7",
    color:"#fff",
    '&:hover':{
      background:"#337ab7"
    }
  },
  content:{
    display:'flex',
  },
  form:{
    padding:'0 20px',
    flexGrow:1,
  }
})

/**
 * tsx的三种写法
 * 1.定义成渲染函数
 * 2.使用选项式api(defineComponent(data\render))
 * 3.setup函数(defineComponent(setup))
 */
export default defineComponent({
  setup(){
    interface StateType{
      schema: any|null;
      data: any;
      uiSchema: any|null;
      schemaCode: string;
      dataCode: string;
      uiSchemaCode: string;
    }

    //初始化可传递参数
    const state:StateType = reactive({
      schema:null,
      data:{},
      uiSchema:{},
      schemaCode:'',
      dataCode:'',
      uiSchemaCode:""
    })

    const selectedRef:Ref<number> = ref(0);

    //监听选中组件的变化，得到当前组件的参数
    watchEffect(()=>{
      const index = selectedRef.value
      const d = demos[index];
      state.schema = d.schema;
      state.data = d.default;
      state.uiSchema = d.uiSchema
      state.schemaCode = toJson(d.schema)
      state.dataCode = toJson(d.default)
      state.uiSchemaCode = toJson(d.uiSchema);
    })

    const classesRef = useStyles()

    //回调的具体处理(编辑器回调回来的参数进行处理赋值)
    const handleChange = (type:'schema'|'data'|'uiSchema',value:string)=>{
      try{
        const json = JSON.parse(value);
        state[type] = json;
        state[`${type}Code`] = value
      }catch(err){
        console.log(err)
      }
    }

    //回调
    const handleSchemaChange = (v:string)=>handleChange('schema',v)
    const handleUISchemaChange = (v:string)=>handleChange('uiSchema',v)
    const handleDataChange = (v:string)=>handleChange('data',v)

    //表单值发生变化
    const handleFormChange = (value:any)=>{
      state.data = value
      state.dataCode = toJson(value)
    }

    return()=>{
      //样式
      const classes = classesRef.value
      //选择的组件
      const selected = selectedRef.value

      return(
        <div class={classes.container}>
          <div class={classes.menu}>
            <h1>Vue3 Json Schema Form</h1>
            <div>
              {
                demos.map((item,index)=>{
                  return (
                    <button 
                      class={{
                        [classes.menuButton]:true,
                        [classes.menuSelected]:index==selected,
                      }}
                      onClick = {()=>(selectedRef.value = index)}
                    >
                      {item.name}
                    </button>
                  )
                })
              }
            </div>
          </div>
          <div class={classes.content}>
            <div class={classes.code}>
              <MonacoEditor 
                title="Schema" 
                class={classes.codePanel} 
                onChange={handleSchemaChange}
                code={state.schemaCode}/>
              <div class={classes.uiAndValue}>
                <MonacoEditor 
                  title="UISchema" 
                  class={classes.codePanel} 
                  onChange={handleUISchemaChange}
                  code={state.uiSchemaCode}/>
                <MonacoEditor
                  code={state.dataCode}
                  onChange={handleDataChange}
                  class={classes.codePanel}
                  title="Value"
                />
              </div>
            </div>
            <div class={classes.form}>
              <SchemaForm 
                schema={state.schema} 
                onChange={handleFormChange}
                value={state.data}
              />
            </div>
          </div>
        </div>
      )
    }
  }
})